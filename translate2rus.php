﻿<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<html>
	<head>
	<meta charset="utf-8">
	</head>
	<body>
	
	<?php
		$rus_checked = ($_GET["action"]=="rus")?"checked='checked'":"";
		$eng_checked = ($_GET["action"]=="eng")?"checked='checked'":"";
	?>
	
		<form>
			<b>Выберите язык: </b><br>
			<input type="radio" name="action" value="rus" <?php echo $rus_checked;?>>Переревести на русский</input><br>
			<input type="radio" name="action" value="eng" <?php echo $eng_checked;?>>Перевести на english</input><br>
			<hr><b>Текст:</b>
			<input type="text" name="text"/><br>
			<p style="text-align: left">
			<button>Перевести</button><br><br>
			
			
			
		</form>
	
	<?php
	
		function isSymbInString($str, $symb){
			for ($i=0;$i<strlen($str);$i++){
				$curr_symb = mb_substr($str, $i, 1, 'utf-8');
				if ($curr_symb == $symb)
					return true;
			}
			return false;
		}
	
		$lang = $_GET["action"];
		$text = $_GET["text"];
		
		$rus_keys = "ёйцукенгшщзхъфывапролджэячсмитьбю.ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ.";
		$eng_keys = "`qwertyuiop[]asdfghjkl;'zxcvbnm,./`QWERTYUIOP[]ASDFGHJKL;'ZXCVBNM,./";
		
		$rus_to_eng_table = array();
		$eng_to_rus_table = array();
		
		for ($i=0;$i<strlen($rus_keys);$i++){
			$rus_to_eng_table[mb_substr($rus_keys, $i, 1, 'utf-8')] = mb_substr($eng_keys, $i, 1, 'utf-8');
			$eng_to_rus_table[mb_substr($eng_keys, $i, 1, 'utf-8')] = mb_substr($rus_keys, $i, 1, 'utf-8');
		}
		
		$result_str = "";
		if ($lang=="rus"){
			for ($i=0;$i<strlen($text);$i++){
				$curr_symb = mb_substr($text, $i, 1, 'utf-8');
				if (isSymbInString($eng_keys, $curr_symb))
					$result_str = $result_str.$eng_to_rus_table[$curr_symb];
				else
					$result_str = $result_str.$curr_symb;
			}
		}elseif ($lang=="eng"){
			for ($i=0;$i<strlen($text);$i++){
				$curr_symb = mb_substr($text, $i, 1, 'utf-8');
				if (isSymbInString($rus_keys, $curr_symb))
					$result_str = $result_str.$rus_to_eng_table[$curr_symb];
				else
					$result_str = $result_str.$curr_symb;
			}
		}
		
		echo $result_str;
		//echo "\n".mb_substr($text, 2, 1, 'utf-8');
		
	?>
	</body>
</html>