<?php
	
	function parseUrl($url){
		try{
			$data = @file_get_contents($url);
		}catch (Exception $ex){
			return null;
		}
		if (strlen($data) < 45 || isExist($data))
			return null;
		$data_obj = new stdClass;
		$data_obj->dev_name = cutText($data,"<span itemprop=\"name\">","</span>");
		$data_obj->app_name = cutText($data,"<div class=\"id-app-title\" tabindex=\"0\">","</div>");
		$data_obj->uid = cutText($data,"data-deep-link-type=\"1\" data-docid=\"","\" data-server");
		return $data_obj;
	}
	
	function isExist($data){
		return explode("<title>Not Found</title>",$data)==1;
	}
	
	function cutText($data,$del1,$del2){
		return explode($del2,explode($del1,$data)[1])[0];
	}

?>