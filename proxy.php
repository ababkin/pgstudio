<?php

		include("connect.php");

        $data = $_POST["data"];

        if ($data=="")
            die("ERROR");

            
            
        $data = json_decode(utf8_encode($data));

        switch (json_last_error()) {
        case JSON_ERROR_NONE:
            echo ' - Ошибок нет';
        break;
        case JSON_ERROR_DEPTH:
            echo ' - Достигнута максимальная глубина стека';
        break;
        case JSON_ERROR_STATE_MISMATCH:
            echo ' - Некорректные разряды или не совпадение режимов';
        break;
        case JSON_ERROR_CTRL_CHAR:
            echo ' - Некорректный управляющий символ';
        break;
        case JSON_ERROR_SYNTAX:
            echo ' - Синтаксическая ошибка, не корректный JSON';
        break;
        case JSON_ERROR_UTF8:
            echo ' - Некорректные символы UTF-8, возможно неверная кодировка';
        break;
        default:
            echo ' - Неизвестная ошибка';
        break;
    }
        

		try {
            $DBH = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
            $DBH->exec('USE '.$db_name.';');
            $DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

            $STH = $DBH->prepare("SET NAMES 'utf8';SET CHARACTER SET 'utf8';SET SESSION collation_connection = 'utf8_general_ci';");
            $STH->execute();
            
            $STH = $DBH->prepare("
            INSERT INTO proxy
              (ip, port, country, iswork)
            VALUES
              (?, ?, ?, ?)
            ON DUPLICATE KEY UPDATE
              port = VALUES(port),
              country = VALUES(country),
              iswork = VALUES(iswork)");
            
            for ($i=0;$i<count($data);$i++){
                $STH->execute(array($data[$i]->ip, $data[$i]->port, $data[$i]->countryCode, $data[$i]->status));
            }
            
            
            echo("\nSuccess");
        }catch(PDOException $e) {
            echo($e->getMessage());
        }
