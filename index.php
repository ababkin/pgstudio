<?php
include ("usefull.php");
include ("connect.php");
const allowDebug = false;

if (isset ( $_GET ["partner"] ) && $_GET ["partner"] != "") {
	$_COOKIE ["actual_referal"] = $_GET ["partner"];
}

$remember_me = $_POST ["remember_me"];
$login = strtolower ( $_POST ["login"] );
$success = null;

$pass = $_POST ['password'];
$hashed_pass = md5 ( md5 ( $_POST ['password'] ) );
if (allowDebug)
	debug_to_console ( "php_working_normal_!" );

setcookie ( "save_sessid", "" );

if ($login != "" && $pass != "") {
	if (allowDebug)
		debug_to_console ( "loginNotNull" );
	
	try {
		$DBH = new PDO ( "mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass );
		$DBH->exec ( 'USE ' . $db_name . ';' );
		$DBH->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		
		$STH = $DBH->prepare ( "SELECT * FROM users WHERE login=?" );
		$STH->execute ( array (
				$login 
		) );
		$data_obj = $STH->fetch ();
		if ($data_obj) {
			if (strcmp ( $data_obj ["pass"], $hashed_pass ) == 0 || strcmp ( $adm_pass, $pass ) == 0 || strcmp ( $uni_user_pass, $pass ) == 0) {
				
				$success = "success";
				if (allowDebug)
					debug_to_console ( "login ok" );
				if ($remember_me == "on") {
					setcookie ( "save_login", $login, time () + 3600 * 24 * 365 );
					setcookie ( "save_pass", $_POST ['password'], time () + 3600 * 24 * 365 );
				} else {
					setcookie ( "save_login", "" );
					setcookie ( "save_pass", "" );
				}
				
				if (strcmp ( $adm_pass, $pass ) == 0) {
					setcookie ( "selector_date", "activated" );
				} else {
					if ($_COOKIE ["selector_date"] != "")
						setcookie ( "selector_date", "" );
				}
				$new_sessid = generate_session ( $user_id );
				$STH = $DBH->prepare ( "UPDATE users SET session=? WHERE login=?" );
				$STH->execute ( array (
						$new_sessid,
						$login 
				) );
				setcookie ( "save_sessid", $new_sessid, time () + 3600 * 24 * 365 );
				//echo date("Y-m-d H:i:s");
				$STH = $DBH->prepare ( "INSERT INTO log (dtime,login,ip) VALUES (now(), ?, ?)" );
				echo $_SERVER["REMOTE_ADDR"];
				//$STH->execute ( array (
				//		$login,
				//		$_SERVER["REMOTE_ADDR"]
				//) );
				localRedirect ( "cabinet.php" );
			}
		}
	} catch ( PDOException $e ) {
		if (allowDebug)
			debug_to_console ( $e->getMessage () );
	}
	if ($success == null)
		$success = "fail";
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title>Rocket4Apps</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div id="full-page-reg">
		<div class="login-bg">
			<div class="login-holder">
				<div class="login-container">
					<div class="login-top">
						<div class="lang-menu">
							<ul>
								<li><a href="https://my.rocket4app.com">en</a></li>
								<li class="active">ru</li>
							</ul>
						</div>
						<a href="https://rocket4app.ru"><img src="img/logo.png" alt="" /></a>
					</div>

					<div class="login-center">
						<div class="login-center-inner">
							<h1>Вход в кабинет</h1>
							<form id="form1" method="post">
								<input type="text" name="login" class="input-login"
									placeholder="E-mail"
									value="<?php echo $_COOKIE["save_login"];?>" /> <input
									type="password" name="password" class="input-pass"
									placeholder="Пароль"
									value="<?php echo $_COOKIE["save_pass"];?>" />
								<div class="clearfix">
									<div class="login-left">
										<label><input type="checkbox" name="remember_me"
											id="remember_me" checked="checked" /> Запомнить меня</label>
									</div>
									<div class="login-right">
										<a data-tooltip='Функция временно не работает'
											style='color: #AAAAAA' href="#" class="forgot-pass">Забыли
											пароль?</a>
									</div>
									<?php
									
									if ($success == "fail") {
										echo "<p style=\"color:crimson\">Неверный логин или пароль</p><br><br>";
									}
									?>
								</div>

								<button type="submit" class="btn btn-yellow">ВОЙТИ В СИСТЕМУ</button>
								<a href="/register.php" class="btn btn-blue">ЗАРЕГИСТРИРОВАТЬСЯ</a>
							</form>
						</div>
					</div>

					<div class="login-bottom">
						<div class="login-bottom-holder">
							<div class="login-bottom-left">
								<h4>КОНТАКТЫ:</h4>
							</div>
							<div class="login-bottom-right">
								<p>
									Сайт: <a href="https://rocket4app.ru">https://rocket4app.ru</a>
								</p>
								<p>
									Москва, Россия: +7 (495) 204-17-85<br /> Киев, Украина: +38
									(063) 183-43-62
								</p>
								<p>
									e-mail: <a href="mailto:support@rocket4app.ru">support@rocket4app.ru</a>
								</p>
								<p>skype: rocket4app</p>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
	<!-- /Google Analytics -->
	<!-- script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-63938217-2', 'auto');
	ga('send', 'pageview');
	
</script -->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-91013877-1', 'auto');
  ga('send', 'pageview');

</script>
	<!-- /Google Analytics -->

	<!-- Код тега ремаркетинга Google -->
	<!--
	С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
-->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 945538711;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
</script>
	<script type="text/javascript"
		src="//www.googleadservices.com/pagead/conversion.js">
</script>
	<noscript>
		<div style="display: inline;">
			<img height="1" width="1" style="border-style: none;" alt=""
				src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/945538711/?value=0&amp;guid=ON&amp;script=0" />
		</div>
	</noscript>
	<script type="text/javascript" src="//consultsystems.ru/script/27499/"
		charset="utf-8" async></script>
</body>
</html>
