jQuery(document).ready(function($) {
	
	$('#full-page').css('min-height', $(window).height() + 'px').css('padding-bottom', $('.site-footer').height() + 'px');
	
	$(window).resize(function() {
		setTimeout(function(){
			$('#full-page').css('min-height', $(window).height() + 'px').css('padding-bottom', $('.site-footer').height() + 'px');
		}, 400);
		
	});
	
	$('.toggle-menu').on('click', function(e) {
		e.preventDefault();
		if($('.mobile-menu').is(':visible')) {
			$(this).removeClass('active');
			$('.mobile-menu').velocity("slideUp", { duration: 300 });
		} else {
			$(this).addClass('active');
			$('.mobile-menu').velocity("slideDown", { duration: 300 });
		}
		return false;
	});
	
	if ($(".tabs").length) {
		$(".tabs").tabslet({
				
		});
	}
	
	if ($.fn.basictable) {
		$('.responsive-table').basictable({
			breakpoint: 959
		});
		
	}
	
	if($('.select-date-range').length) {
		$('.select-date-range').dateRangePicker({
			separator: ' - ',
			startOfWeek: 'monday'
		});
	}
	
	$("input.for-copy").click(function() { 
		$(this).focus().select(); 
		
	});
	
});
//# sourceMappingURL=../maps/functions.js.map
