<?php
include ("usefull.php");
include ("connect.php");
include ("parts.php");

$user_data = null;
$transactions_data = null;

$money_adm = isset ( $_POST ["money"] ) ? $_POST ["money"] : "0";

// init transactions codes dictionary
$codes = array ();

$code_error = array ();
$code_error ["id"] = 0;
$code_error ["name"] = "error";
$code_error ["message_ru"] = "Ошибка";

$code_income = array ();
$code_income ["id"] = 1;
$code_income ["name"] = "casual_income";
$code_income ["message_ru"] = "Пополнение баланса";

$code_income ["id"] = 2;
$code_income ["name"] = "bonus_income";
$code_income ["message_ru"] = "Бонус 10%";

$payment_result = array ();
$payment_result [0] = "Не оплачен";
$payment_result [1] = "Оплачен";
$payment_result [2] = "Зачислен на баланс";
$payment_result [3] = "Снят с баланса";

$codes_income = array ();
$codes_income [1] ["message_ru"] = "Пополнение баланса";
$codes_income [2] ["message_ru"] = "Бонус 10% за комиссию Robokassa";
$codes_income [3] ["message_ru"] = "Бонус 20% за Акцию 'Майский старт'";
$codes_income [4] ["message_ru"] = "Покупка услуги 'Топ Категории'";

$codes [] = $code_error;
$codes [] = $code_income;

$sessid = isset ( $_COOKIE ["save_sessid"] ) ? $_COOKIE ["save_sessid"] : "";

if ($sessid != "") {
	try {
		$DBH = new PDO ( "mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass );
		$DBH->exec ( 'USE ' . $db_name . ';' );
		$DBH->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		
		$STH = $DBH->prepare ( "SET NAMES 'utf8';SET CHARACTER SET 'utf8';SET SESSION collation_connection = 'utf8_general_ci';" );
		$STH->execute ();
		
		$STH = $DBH->prepare ( "SELECT * FROM users WHERE session=?" );
		$STH->execute ( array (
				$sessid 
		) );
		$data_obj = $STH->fetch ();
		
		if ($data_obj) {
			$user_data = $data_obj;
			
			// get sum of new payments
			$STH = $DBH->prepare ( "SELECT SUM(value) FROM transactions WHERE user=? AND result=1" );
			$STH->execute ( array (
					$user_data ["id"] 
			) );
			$data_obj = $STH->fetch ();
			$money = $data_obj [0];
			if ($money > 0) {
				// set new payments done
				$STH = $DBH->prepare ( "UPDATE transactions SET result=2 WHERE user=? AND result=1" );
				$STH->execute ( array (
						$user_data ["id"] 
				) );
				
				// !!! +++ Bonus +++ !!!
				$bonus = 0;//$money * 0.2;
				
				// Updating user balance
				$STH = $DBH->prepare ( "UPDATE users SET balance=balance+?, referal_income=referal_income+? WHERE id=?" );
				$STH->execute ( array (
						$money + $bonus,
						$money * 0.15,
						$user_data ["id"] 
				) );
				// Updating referal balance
				$STH = $DBH->prepare ( "UPDATE users SET balance=balance+? WHERE id=?" );
				$STH->execute ( array (
						$money * 0.15,
						$user_data ["referal"] 
				) );
				
				/** Add bonus to user
				$STH = $DBH->prepare ( "INSERT INTO transactions (user, code, value, date, inv_id, result) VALUES (?,?,?,?,?,?)" );
				$STH->execute ( array (
						$user_data ["id"],
						"3",
						$bonus,
						date ( 'Y-m-d' ),
						$_REQUEST ["InvId"] + 1,
						"2" 
				) );*/
			}
			
			// "selector_date" god cookie
			if (isset ( $money_adm ) && $money_adm != "" && $money_adm != "0" && $_COOKIE ["selector_date"] == "activated") {
				
				$STH = $DBH->prepare ( "SELECT MAX(inv_id) FROM transactions" );
				$STH->execute ();
				$data_obj = $STH->fetch ();
				
				$inv_id = $data_obj [0] + 2;
				
				// logging balance
				$STH = $DBH->prepare ( "INSERT INTO transactions (user, code, value, date, inv_id, result) VALUES (?,?,?,?,?,?)" );
				$STH->execute ( array (
						$user_data ["id"],
						"1",
						$money_adm,
						date ( 'Y-m-d' ),
						$inv_id,
						"2" 
				) );
				
				// Updating user balance
				$STH = $DBH->prepare ( "UPDATE users SET balance=balance+?, referal_income=referal_income+? WHERE id=?" );
				$STH->execute ( array (
						$money_adm,
						$money_adm * 0.15,
						$user_data ["id"] 
				) );
				// Updating referal balance
				$STH = $DBH->prepare ( "UPDATE users SET balance=balance+? WHERE id=?" );
				$STH->execute ( array (
						$money_adm * 0.15,
						$user_data ["referal"] 
				) );
			}
			
			// refreshing userdata
			$STH = $DBH->prepare ( "SELECT * FROM users WHERE session=?" );
			$STH->execute ( array (
					$sessid 
			) );
			$data_obj = $STH->fetch ();
			$user_data = $data_obj;
			
			$STH = $DBH->prepare ( "SELECT * FROM transactions WHERE user=? ORDER BY id DESC" );
			$STH->execute ( array (
					$user_data ["id"] 
			) );
			$transactions_data = $STH->fetchAll ();
		} else {
			localRedirect ( "/" );
		}
	} catch ( PDOException $e ) {
		echo ($e->getMessage ());
	}
} else {
	localRedirect ( "/" );
}
?>

<?php echo getHeader($user_data["firstname"],$user_data["balance"], "balance"); ?>

<script>
		$(document).ready(function() {

			// If cookie is set, scroll to the position saved in the cookie.
			if ( $.cookie("scroll") !== null ) {
				$(document).scrollTop( $.cookie("scroll") );
				$.cookie("scroll", null);
			}

			// When a button is clicked...
			$('#submit').on("click", function() {

				// Set a cookie that holds the scroll position.
				$.cookie("scroll", $(document).scrollTop() );

			});

		});
	</script>


<div class="content-section">
	<div class="cs-head">
		<h2>ПОПОЛНИТЬ БАЛАНС</h2>
	</div>
	<div class="cs-body white-bg">
		<form method="POST"
			action="<?php echo ($_COOKIE["selector_date"]=="activated") ? "" : '/robokassa.php'; ?>">
			<div class="payment-options">
				<!--
				<p>В данный момент платежи принимаются в ручном режиме через оператора.</p>
				<p>Skype: rocket4app</p>
				<p>E-mail: support@rocket4app.ru</p>
				-->

				<div class="payment-option">
					<label> <input type="radio" name="payment_method" value="1" />
						<div class="payment-image">
							<img src="img/payment1.png" alt="" />
						</div>
					</label>
				</div>

				<div class="payment-option">
					<label> <input type="radio" name="payment_method" value="2"
						checked="checked" />
						<div class="payment-image">
							<img src="img/payment2.png" alt="" />
						</div>
					</label>
				</div>

				<div class="payment-option">
					<label> <input type="radio" name="payment_method" value="3" />
						<div class="payment-image">
							<img src="img/payment3.png" alt="" />
						</div>
					</label>
				</div>

				<div class="payment-option">
					<label> <input type="radio" name="payment_method" value="4" />
						<div class="payment-image">
							<img src="img/payment4.png" alt="" />
						</div>
					</label>
				</div>

				<div class="payment-option">
					<label> <input type="radio" name="payment_method" value="5" />
						<div class="payment-image">
							<img src="img/payment5.png" alt="" />
						</div>
					</label>
				</div>
				<!-- 
				<div class="payment-option">
					<label>
					  <input type="radio" name="payment_method" value="6" />
					  <div class="payment-image">
						<img src="img/payment6.png" alt="" />
					  </div>
					</label>
				</div>

				<div class="payment-option">
					<label>
					  <input type="radio" name="payment_method" value="7" />
					  <div class="payment-image">
						<img src="img/payment7.png" alt="" />
					  </div>
					</label>
				</div>

				<div class="payment-option">
					<label>
					  <input type="radio" name="payment_method" value="8" />
					  <div class="payment-image">
						<img src="img/payment8.png" alt="" />
					  </div>
					</label>
				</div>
				 -->
			</div>

			<div class="blue-form bordered wallet-bg">
				<div>
					<label> Сумма пополнения в USD, наш курс 1 USD = 56 рублей<br /> <input
						type="text" class="checkout-amount" name="money"
						placeholder="Введите сумму для пополнения" /> <!--= <span>5746</span> Рублей-->
					</label>
				</div>

				<input type="hidden" name="user_id" value="<?php echo $user_data['id']; ?>" />
				<input type="hidden" name="user_login" value="<?php echo $user_data['login']; ?>" />

				<button type="submit" class="btn btn-blue-square btn-wallet">Пополнить</button>
				<!-- <br /> <br />Акция 'Майский старт': Мы дарим вам +20% от платежа на
				баланс! Действует до 5 мая. -->
			</div>

		</form>
		<!--
		<script>
				$( "form" ).submit(function( event ) {
					$.cookie("scroll", $(document).scrollTop() );
				});
			</script>-->
	</div>
</div>

<div class="content-section" id="history">
	<div class="cs-head">
		<h2>ИСТОРИЯ ОПЕРАЦИЙ</h2>
	</div>
	<div class="cs-body white-bg">
		<table class="striped-table responsive-table">
			<tr>
				<th>Дата</th>
				<th>Сумма</th>
				<th>Операция</th>
				<th>Статус</th>
			</tr>
			<?php
			
			for($i = 0; $i < count ( $transactions_data ); $i ++) {
				echo getTransactionsPart ( $transactions_data [$i] ["date"], $transactions_data [$i] ["value"], $codes_income [$transactions_data [$i] ["code"]] ["message_ru"], $payment_result [$transactions_data [$i] ["result"]] );
			}
			
			?>

		</table>
	</div>
</div>

<?php echo getFooter(); ?>
